import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.print.DocFlavor;
import java.time.Duration;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class HomeWork {

    WebDriver driver;
    WebDriverWait wait;
    JavascriptExecutor js;
    Actions action ;

    @BeforeMethod
    public void setUp() {
        driver = new ChromeDriver();
        js = (JavascriptExecutor) driver;
        action = new Actions(driver);
        wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        driver.get("https://demoqa.com/");
        driver.manage().window().maximize();

    }

    //Task 1

    @Test
    public void titleOfPage() {
        String title = driver.getTitle();
        Assert.assertEquals(title, "DEMOQA", "Title not correct!");

    }

    //Task 2

    @Test
    public void submitInformation () {
        List<WebElement> listOfCards = driver.findElements(By.cssSelector(".category-cards>div"));
        WebElement cardElement = listOfCards.get(0);
        js.executeScript("arguments[0].scrollIntoView();", cardElement);
        cardElement.click();
        List<WebElement> listOfItems = driver.findElements(By.cssSelector("ul>#item-0"));
        WebElement item = listOfItems.get(0);
        item.click();

        WebElement fullName= driver.findElement(By.cssSelector("#userName"));
        fullName.sendKeys("Nigar Gulmammadova");
        WebElement email = driver.findElement(By.cssSelector("#userEmail"));
        email.sendKeys("nigargulmammadova95@gmail.com");
        WebElement currentAdress = driver.findElement(By.cssSelector("#currentAddress"));
        currentAdress.sendKeys("Baku, Azerbaijan");
        WebElement permanentAdress= driver.findElement(By.cssSelector("#permanentAddress"));
        permanentAdress.sendKeys("London");
        WebElement submitBtn = driver.findElement(By.cssSelector("#submit"));
        js.executeScript("arguments[0].scrollIntoView();", submitBtn);
        submitBtn.click();

        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        WebElement submittedName = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("name")));
        WebElement submittedEmail = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("email")));
        WebElement submittedCurrentAdress = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("p#currentAddress")));
        WebElement submittedPermanentAdress = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("p#permanentAddress")));

        Assert.assertEquals(submittedName.getText(), "Name:Nigar Gulmammadova");
        Assert.assertEquals(submittedEmail.getText(), "Email:nigargulmammadova95@gmail.com");
        Assert.assertEquals(submittedCurrentAdress.getText(), "Current Address :Baku, Azerbaijan");
        Assert.assertEquals(submittedPermanentAdress.getText(), "Permananet Address :London");
    }

    //Task  3

    @Test
    public void notes() {
        List<WebElement> listOfCards = driver.findElements(By.cssSelector(".category-cards>div"));
        WebElement cardElement = listOfCards.get(0);
        js.executeScript("arguments[0].scrollIntoView();", cardElement);
        cardElement.click();

        List<WebElement> listOfItems = driver.findElements(By.cssSelector("ul>#item-1"));
        WebElement checkBox = listOfItems.get(0);
        checkBox.click();
        WebElement expand = driver.findElement(By.cssSelector("button[title=\"Expand all\"]"));
        expand.click();
        WebElement notes = driver.findElement(By.cssSelector("label[for=\"tree-node-notes\"]>.rct-checkbox>svg"));
        notes.click();
        WebElement result = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".text-success")));
        Assert.assertTrue(result.isDisplayed());
    }


    //Task 4

    @Test
    public void rightClickMe () {
        List<WebElement> listOfCards = driver.findElements(By.cssSelector(".category-cards>div"));
        WebElement cardElement = listOfCards.get(0);
        js.executeScript("arguments[0].scrollIntoView();", cardElement);
        cardElement.click();
        List<WebElement> listOfItems = driver.findElements(By.cssSelector("ul>#item-4"));
        WebElement button = listOfItems.get(0);
        button.click();
        WebElement rightClickBtn = driver.findElement(By.cssSelector("#rightClickBtn"));
        Actions action = new Actions(driver);
        action.contextClick(rightClickBtn).build().perform();
        WebElement text = driver.findElement(By.cssSelector("#rightClickMessage"));
        Assert.assertTrue(text.isDisplayed());
    }

    //Task 5

    @Test
    public void fileUpload (){
        List<WebElement> listOfCards = driver.findElements(By.cssSelector(".category-cards>div"));
        WebElement cardElement = listOfCards.get(0);
        js.executeScript("arguments[0].scrollIntoView();", cardElement);
        cardElement.click();
        List<WebElement> listOfItems = driver.findElements(By.cssSelector("ul>#item-7"));
        WebElement fileUpload = listOfItems.get(0);
        js.executeScript("arguments[0].scrollIntoView();", fileUpload);
        fileUpload.click();
        WebElement fileUploadBtn = driver.findElement(By.cssSelector("#uploadFile"));
        fileUploadBtn.sendKeys("C:\\Users\\vuqar\\AppData\\Local\\JetBrains\\IntelliJ IDEA Community Edition 2023.1.2\\jbr\\bin\\Desktop\\First project\\HomeWork16\\src\\main\\resources");
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#uploadedFilePath")));
        WebElement text = driver.findElement(By.cssSelector("#uploadedFilePath"));
        Assert.assertTrue(text.isDisplayed());

    }

    //Task 6

    @Test
    public void dynamicProperties () {
        List<WebElement> listOfCards = driver.findElements(By.cssSelector(".category-cards>div"));
        WebElement cardElement = listOfCards.get(0);
        js.executeScript("arguments[0].scrollIntoView();", cardElement);
        cardElement.click();
        List<WebElement> listItems = driver.findElements(By.cssSelector("ul>#item-8"));
        WebElement DynamicProperties = listItems.get(0);
        DynamicProperties.click();
        WebElement visibleAfter5Seconds = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#visibleAfter")));
        Assert.assertEquals(visibleAfter5Seconds.getText(), "Visible After 5 Seconds");

    }

    //Task 7

    @Test
    public void alertNewTab (){
        List<WebElement> listOfCars = driver.findElements(By.cssSelector(".category-cards>div"));
        WebElement cardAlert = listOfCars.get(2);
        js.executeScript("arguments[0].scrollIntoView();", cardAlert);
        cardAlert.click();
        List<WebElement> listOfItems  = driver.findElements(By.cssSelector(".menu-list>#item-0"));
        WebElement browser= listOfItems.get(2);
        browser.click();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        WebElement newTabBtn = driver.findElement(By.cssSelector("#tabButton"));
        newTabBtn.click();
        String mainWindow = driver.getWindowHandle();
        Set<String> windowsId = driver.getWindowHandles();
        Iterator<String> iterator = windowsId.iterator();
        while (iterator.hasNext()) {
            String childWindow = iterator.next();
            if (!mainWindow.equalsIgnoreCase(childWindow)) {
                driver.switchTo().window(childWindow);
                WebElement h1 = driver.findElement(By.cssSelector("#sampleHeading"));
                System.out.println(h1.getText());
                driver.close();
            }
        }

    }

    //Task 8

    @Test
    public void promtBox () {
        List<WebElement> listOfCars = driver.findElements(By.cssSelector(".category-cards>div"));
        WebElement cardAlert = listOfCars.get(2);
        js.executeScript("arguments[0].scrollIntoView();", cardAlert);
        cardAlert.click();
        List<WebElement> listOfItems  = driver.findElements(By.cssSelector(".menu-list>#item-1"));
        WebElement alerts= listOfItems.get(1);
        alerts.click();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        WebElement promptButton =driver.findElement(By.cssSelector("#promtButton"));
        promptButton.click();
        driver.switchTo().alert().sendKeys("Test");
        driver.switchTo().alert().accept();
        WebElement enteredText = driver.findElement(By.cssSelector(".text-success"));
        wait.until(ExpectedConditions.visibilityOf(enteredText));
        Assert.assertTrue(enteredText.isDisplayed());
    }

    //Task 9

    @Test
    public void confirmBox (){
        List<WebElement> listOfCars = driver.findElements(By.cssSelector(".category-cards>div"));
        WebElement cardAlert = listOfCars.get(2);
        js.executeScript("arguments[0].scrollIntoView();", cardAlert);
        cardAlert.click();
        List<WebElement> listOfItems  = driver.findElements(By.cssSelector(".menu-list>#item-1"));
        WebElement alerts= listOfItems.get(1);
        alerts.click();
        WebElement confirmBtn = driver.findElement(By.cssSelector("#confirmButton"));
        confirmBtn.click();
        driver.switchTo().alert().dismiss();
        WebElement message = driver.findElement(By.cssSelector(".text-success"));
        wait.until(ExpectedConditions.visibilityOf(message));
        Assert.assertTrue(message.isDisplayed());
    }

    //Task 10

    @Test
    public void frame () {
        List<WebElement> listOfCars = driver.findElements(By.cssSelector(".category-cards>div"));
        WebElement cardAlert = listOfCars.get(2);
        js.executeScript("arguments[0].scrollIntoView();", cardAlert);
        cardAlert.click();
        List<WebElement> listOfItems  = driver.findElements(By.cssSelector("ul>#item-2"));
        WebElement frames= listOfItems.get(1);
        frames.click();
        WebElement outFrame = driver.findElement(By.cssSelector("#frame1"));
        driver.switchTo().frame(outFrame);
        WebElement text = driver.findElement(By.cssSelector("#sampleHeading"));
        Assert.assertTrue(text.isDisplayed());
    }

    //Task 11

    @Test
    public void Selectable (){
        List<WebElement> listOfCards = driver.findElements(By.cssSelector(".category-cards>div"));
        WebElement cardInteractions = listOfCards.get(4);
        js.executeScript("arguments[0].scrollIntoView();", cardInteractions);
        cardInteractions.click();
       List<WebElement> listOfItems = driver.findElements(By.cssSelector(".menu-list>#item-1"));
        WebElement selectable = listOfItems.get(3);
        selectable.click();
        WebElement grid = driver.findElement(By.cssSelector("#demo-tab-grid"));
        grid.click();
        List<WebElement> gridList = driver.findElements(By.cssSelector("#row2>li"));
        WebElement five = gridList.get(1);
        five.click();
        wait.until(ExpectedConditions.attributeToBe(five, "class", "list-group-item active list-group-item-action"));
        Assert.assertEquals(five.getAttribute("class"), "list-group-item active list-group-item-action");
    }

    //Task 12

    @Test
    public void Droppable (){
        List<WebElement> listOfCards = driver.findElements(By.cssSelector(".category-cards>div"));
        WebElement cardInteractions = listOfCards.get(4);
        js.executeScript("arguments[0].scrollIntoView();", cardInteractions);
        cardInteractions.click();
        List<WebElement> listOfItems= driver.findElements(By.cssSelector(".menu-list>#item-3"));
        WebElement droppable = listOfItems.get(3);
        droppable.click();
        WebElement dragMe = driver.findElement(By.cssSelector("#draggable"));
        WebElement dropHere = driver.findElement(By.cssSelector("div.simple-drop-container>div#droppable"));
        action.dragAndDrop(dragMe, dropHere).build().perform();
        WebElement textDropped = driver.findElement(By.cssSelector("div.drop-box.ui-droppable.ui-state-highlight>p"));
        Assert.assertTrue(textDropped.isDisplayed());
    }

  @AfterMethod
    public void tearDown() {
        driver.quit();}
}
